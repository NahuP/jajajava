package objetosEnClase.controller.composite;

public class FiguraNombreVacioComposite extends ValidatorComposite {

	public FiguraNombreVacioComposite() {}

	@Override
	public boolean isMe() {		
		return true;
	}
	@Override
	public String getError() {
		return "El nombre no debe estar vacio";
	}
	@Override
	public boolean validar() {
		return figura.getNombre().isEmpty();
	}
}
