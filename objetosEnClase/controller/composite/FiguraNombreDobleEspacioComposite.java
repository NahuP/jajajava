package objetosEnClase.controller.composite;

public class FiguraNombreDobleEspacioComposite extends ValidatorComposite {

	public FiguraNombreDobleEspacioComposite() {}

	@Override
	public boolean isMe() {		
		return true;
	}
	@Override
	public String getError() {		
		return "El nombre no debe tener dos espacios";
	}
	@Override
	public boolean validar() {		
		return figura.getNombre().contains("  ");
	}
}
